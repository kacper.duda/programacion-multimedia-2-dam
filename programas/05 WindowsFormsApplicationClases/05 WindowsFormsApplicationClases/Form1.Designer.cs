﻿namespace _05_WindowsFormsApplicationClases
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCirculo1 = new System.Windows.Forms.Button();
            this.buttonCirculo2 = new System.Windows.Forms.Button();
            this.buttonCirculo3 = new System.Windows.Forms.Button();
            this.textBoxC1 = new System.Windows.Forms.TextBox();
            this.textBoxC2 = new System.Windows.Forms.TextBox();
            this.textBoxC3 = new System.Windows.Forms.TextBox();
            this.textBoxR1 = new System.Windows.Forms.TextBox();
            this.textBoxR3 = new System.Windows.Forms.TextBox();
            this.textBoxR2 = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCirculo1
            // 
            this.buttonCirculo1.Location = new System.Drawing.Point(12, 113);
            this.buttonCirculo1.Name = "buttonCirculo1";
            this.buttonCirculo1.Size = new System.Drawing.Size(146, 74);
            this.buttonCirculo1.TabIndex = 0;
            this.buttonCirculo1.Text = "Circulo 1";
            this.buttonCirculo1.UseVisualStyleBackColor = true;
            this.buttonCirculo1.Click += new System.EventHandler(this.buttonCirculo1_Click);
            // 
            // buttonCirculo2
            // 
            this.buttonCirculo2.Location = new System.Drawing.Point(212, 113);
            this.buttonCirculo2.Name = "buttonCirculo2";
            this.buttonCirculo2.Size = new System.Drawing.Size(146, 74);
            this.buttonCirculo2.TabIndex = 1;
            this.buttonCirculo2.Text = "Circulo 2";
            this.buttonCirculo2.UseVisualStyleBackColor = true;
            // 
            // buttonCirculo3
            // 
            this.buttonCirculo3.Location = new System.Drawing.Point(410, 113);
            this.buttonCirculo3.Name = "buttonCirculo3";
            this.buttonCirculo3.Size = new System.Drawing.Size(146, 74);
            this.buttonCirculo3.TabIndex = 2;
            this.buttonCirculo3.Text = "Circulo 3";
            this.buttonCirculo3.UseVisualStyleBackColor = true;
            // 
            // textBoxC1
            // 
            this.textBoxC1.Location = new System.Drawing.Point(12, 76);
            this.textBoxC1.Name = "textBoxC1";
            this.textBoxC1.Size = new System.Drawing.Size(146, 31);
            this.textBoxC1.TabIndex = 3;
            // 
            // textBoxC2
            // 
            this.textBoxC2.Location = new System.Drawing.Point(212, 76);
            this.textBoxC2.Name = "textBoxC2";
            this.textBoxC2.Size = new System.Drawing.Size(146, 31);
            this.textBoxC2.TabIndex = 3;
            // 
            // textBoxC3
            // 
            this.textBoxC3.Location = new System.Drawing.Point(410, 76);
            this.textBoxC3.Name = "textBoxC3";
            this.textBoxC3.Size = new System.Drawing.Size(146, 31);
            this.textBoxC3.TabIndex = 3;
            // 
            // textBoxR1
            // 
            this.textBoxR1.Location = new System.Drawing.Point(12, 39);
            this.textBoxR1.Name = "textBoxR1";
            this.textBoxR1.Size = new System.Drawing.Size(146, 31);
            this.textBoxR1.TabIndex = 3;
            // 
            // textBoxR3
            // 
            this.textBoxR3.Location = new System.Drawing.Point(410, 39);
            this.textBoxR3.Name = "textBoxR3";
            this.textBoxR3.Size = new System.Drawing.Size(146, 31);
            this.textBoxR3.TabIndex = 3;
            // 
            // textBoxR2
            // 
            this.textBoxR2.Location = new System.Drawing.Point(212, 39);
            this.textBoxR2.Name = "textBoxR2";
            this.textBoxR2.Size = new System.Drawing.Size(146, 31);
            this.textBoxR2.TabIndex = 3;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(12, 257);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(161, 25);
            this.labelResultado.TabIndex = 4;
            this.labelResultado.Text = "El resultado es ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 502);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxR3);
            this.Controls.Add(this.textBoxC3);
            this.Controls.Add(this.textBoxR1);
            this.Controls.Add(this.textBoxR2);
            this.Controls.Add(this.textBoxC2);
            this.Controls.Add(this.textBoxC1);
            this.Controls.Add(this.buttonCirculo3);
            this.Controls.Add(this.buttonCirculo2);
            this.Controls.Add(this.buttonCirculo1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCirculo1;
        private System.Windows.Forms.Button buttonCirculo2;
        private System.Windows.Forms.Button buttonCirculo3;
        private System.Windows.Forms.TextBox textBoxC1;
        private System.Windows.Forms.TextBox textBoxC2;
        private System.Windows.Forms.TextBox textBoxC3;
        private System.Windows.Forms.TextBox textBoxR1;
        private System.Windows.Forms.TextBox textBoxR3;
        private System.Windows.Forms.TextBox textBoxR2;
        private System.Windows.Forms.Label labelResultado;
    }
}

