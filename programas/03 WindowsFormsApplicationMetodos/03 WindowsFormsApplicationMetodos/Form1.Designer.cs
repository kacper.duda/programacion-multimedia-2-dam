﻿namespace _03_WindowsFormsApplicationMetodos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSumar1 = new System.Windows.Forms.Button();
            this.textBoxNumero1 = new System.Windows.Forms.TextBox();
            this.textBoxNumero2 = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonSumar2 = new System.Windows.Forms.Button();
            this.buttonSumar3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonSumar1
            // 
            this.buttonSumar1.Location = new System.Drawing.Point(417, 41);
            this.buttonSumar1.Name = "buttonSumar1";
            this.buttonSumar1.Size = new System.Drawing.Size(131, 59);
            this.buttonSumar1.TabIndex = 0;
            this.buttonSumar1.Text = "Sumar V1";
            this.buttonSumar1.UseVisualStyleBackColor = true;
            this.buttonSumar1.Click += new System.EventHandler(this.buttonSumar1_Click);
            // 
            // textBoxNumero1
            // 
            this.textBoxNumero1.Location = new System.Drawing.Point(12, 55);
            this.textBoxNumero1.Name = "textBoxNumero1";
            this.textBoxNumero1.Size = new System.Drawing.Size(161, 29);
            this.textBoxNumero1.TabIndex = 1;
            // 
            // textBoxNumero2
            // 
            this.textBoxNumero2.Location = new System.Drawing.Point(236, 55);
            this.textBoxNumero2.Name = "textBoxNumero2";
            this.textBoxNumero2.Size = new System.Drawing.Size(161, 29);
            this.textBoxNumero2.TabIndex = 2;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultado.Location = new System.Drawing.Point(12, 446);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(208, 31);
            this.labelResultado.TabIndex = 3;
            this.labelResultado.Text = "El resultado es: ";
            // 
            // buttonSumar2
            // 
            this.buttonSumar2.Location = new System.Drawing.Point(554, 41);
            this.buttonSumar2.Name = "buttonSumar2";
            this.buttonSumar2.Size = new System.Drawing.Size(132, 59);
            this.buttonSumar2.TabIndex = 4;
            this.buttonSumar2.Text = "Sumar V2";
            this.buttonSumar2.UseVisualStyleBackColor = true;
            this.buttonSumar2.Click += new System.EventHandler(this.buttonSumar2_Click);
            // 
            // buttonSumar3
            // 
            this.buttonSumar3.Location = new System.Drawing.Point(692, 41);
            this.buttonSumar3.Name = "buttonSumar3";
            this.buttonSumar3.Size = new System.Drawing.Size(132, 59);
            this.buttonSumar3.TabIndex = 5;
            this.buttonSumar3.Text = "Sumar V3";
            this.buttonSumar3.UseVisualStyleBackColor = true;
            this.buttonSumar3.Click += new System.EventHandler(this.buttonSumar3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 682);
            this.Controls.Add(this.buttonSumar3);
            this.Controls.Add(this.buttonSumar2);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxNumero2);
            this.Controls.Add(this.textBoxNumero1);
            this.Controls.Add(this.buttonSumar1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSumar1;
        private System.Windows.Forms.TextBox textBoxNumero1;
        private System.Windows.Forms.TextBox textBoxNumero2;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonSumar2;
        private System.Windows.Forms.Button buttonSumar3;
    }
}

