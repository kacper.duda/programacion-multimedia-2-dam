﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_WindowsFormsApplicationRestaurante
{
    public enum EstadoMesa { Libre, Reservada, Ocupada };

    class Mesa
    {
        // ATRIBUTOS
        private int nOcupantesMax,
                    nOcupantesAct;
        private EstadoMesa estado;
        private string horaOcupacion;
        
        // CONSTRUCTORES
        public Mesa()
        {
            nOcupantesMax = 4;
            nOcupantesAct = 0;
            estado = EstadoMesa.Libre;
            horaOcupacion = "";
        }

        public Mesa(int nOcMax, int nOcAct, EstadoMesa e, string horaOcup)
        {
            nOcupantesMax = nOcMax;
            nOcupantesAct = nOcAct;
            estado = e;
            horaOcupacion = horaOcup;
        }

        // OCUPAR, COBRAR
        public void Ocupar(int nOcupantes, string horaOcup)
        {
            if ( (estado == EstadoMesa.Libre) && (nOcupantes <= nOcupantesMax) )
            {
                estado = EstadoMesa.Ocupada;
                nOcupantesAct = nOcupantes;
                horaOcupacion = horaOcup;
            }
        }

        public void Cobrar()
        {
            if ( estado == EstadoMesa.Ocupada )
            {
                estado = EstadoMesa.Libre;
                horaOcupacion = "";
            }
        }
    }
}
