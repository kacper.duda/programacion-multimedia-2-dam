﻿namespace _06_WindowsFormsApplicationClasesEjer
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonL1 = new System.Windows.Forms.Button();
            this.buttonL2 = new System.Windows.Forms.Button();
            this.buttonL1Vacio = new System.Windows.Forms.Button();
            this.buttonL2Vacio = new System.Windows.Forms.Button();
            this.buttonL2Inverso = new System.Windows.Forms.Button();
            this.buttonL1MetodoSacar = new System.Windows.Forms.Button();
            this.buttonL1InversoMetodoSacar = new System.Windows.Forms.Button();
            this.buttonL1NumerosPares = new System.Windows.Forms.Button();
            this.buttonL1NumerosImpares = new System.Windows.Forms.Button();
            this.buttonL2UnicoDigito = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultado.Location = new System.Drawing.Point(438, 33);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(161, 50);
            this.labelResultado.TabIndex = 0;
            this.labelResultado.Text = "El resultado es:\r\n\r\n";
            // 
            // buttonL1
            // 
            this.buttonL1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL1.Location = new System.Drawing.Point(18, 12);
            this.buttonL1.Name = "buttonL1";
            this.buttonL1.Size = new System.Drawing.Size(168, 66);
            this.buttonL1.TabIndex = 1;
            this.buttonL1.Text = "A";
            this.buttonL1.UseVisualStyleBackColor = true;
            this.buttonL1.Click += new System.EventHandler(this.buttonL1_Click);
            // 
            // buttonL2
            // 
            this.buttonL2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL2.Location = new System.Drawing.Point(18, 84);
            this.buttonL2.Name = "buttonL2";
            this.buttonL2.Size = new System.Drawing.Size(168, 66);
            this.buttonL2.TabIndex = 2;
            this.buttonL2.Text = "B";
            this.buttonL2.UseVisualStyleBackColor = true;
            this.buttonL2.Click += new System.EventHandler(this.buttonL2_Click);
            // 
            // buttonL1Vacio
            // 
            this.buttonL1Vacio.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL1Vacio.Location = new System.Drawing.Point(18, 156);
            this.buttonL1Vacio.Name = "buttonL1Vacio";
            this.buttonL1Vacio.Size = new System.Drawing.Size(168, 66);
            this.buttonL1Vacio.TabIndex = 3;
            this.buttonL1Vacio.Text = "C";
            this.buttonL1Vacio.UseVisualStyleBackColor = true;
            this.buttonL1Vacio.Click += new System.EventHandler(this.buttonL1Vacio_Click);
            // 
            // buttonL2Vacio
            // 
            this.buttonL2Vacio.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL2Vacio.Location = new System.Drawing.Point(18, 228);
            this.buttonL2Vacio.Name = "buttonL2Vacio";
            this.buttonL2Vacio.Size = new System.Drawing.Size(168, 66);
            this.buttonL2Vacio.TabIndex = 4;
            this.buttonL2Vacio.Text = "D";
            this.buttonL2Vacio.UseVisualStyleBackColor = true;
            this.buttonL2Vacio.Click += new System.EventHandler(this.buttonL2Vacio_Click);
            // 
            // buttonL2Inverso
            // 
            this.buttonL2Inverso.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL2Inverso.Location = new System.Drawing.Point(18, 300);
            this.buttonL2Inverso.Name = "buttonL2Inverso";
            this.buttonL2Inverso.Size = new System.Drawing.Size(168, 66);
            this.buttonL2Inverso.TabIndex = 5;
            this.buttonL2Inverso.Text = "E";
            this.buttonL2Inverso.UseVisualStyleBackColor = true;
            this.buttonL2Inverso.Click += new System.EventHandler(this.buttonL2Inverso_Click);
            // 
            // buttonL1MetodoSacar
            // 
            this.buttonL1MetodoSacar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL1MetodoSacar.Location = new System.Drawing.Point(18, 372);
            this.buttonL1MetodoSacar.Name = "buttonL1MetodoSacar";
            this.buttonL1MetodoSacar.Size = new System.Drawing.Size(168, 66);
            this.buttonL1MetodoSacar.TabIndex = 6;
            this.buttonL1MetodoSacar.Text = "F";
            this.buttonL1MetodoSacar.UseVisualStyleBackColor = true;
            // 
            // buttonL1InversoMetodoSacar
            // 
            this.buttonL1InversoMetodoSacar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL1InversoMetodoSacar.Location = new System.Drawing.Point(18, 444);
            this.buttonL1InversoMetodoSacar.Name = "buttonL1InversoMetodoSacar";
            this.buttonL1InversoMetodoSacar.Size = new System.Drawing.Size(168, 66);
            this.buttonL1InversoMetodoSacar.TabIndex = 7;
            this.buttonL1InversoMetodoSacar.Text = "G";
            this.buttonL1InversoMetodoSacar.UseVisualStyleBackColor = true;
            // 
            // buttonL1NumerosPares
            // 
            this.buttonL1NumerosPares.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL1NumerosPares.Location = new System.Drawing.Point(18, 516);
            this.buttonL1NumerosPares.Name = "buttonL1NumerosPares";
            this.buttonL1NumerosPares.Size = new System.Drawing.Size(168, 66);
            this.buttonL1NumerosPares.TabIndex = 8;
            this.buttonL1NumerosPares.Text = "Lista 10 Numero Pares";
            this.buttonL1NumerosPares.UseVisualStyleBackColor = true;
            // 
            // buttonL1NumerosImpares
            // 
            this.buttonL1NumerosImpares.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL1NumerosImpares.Location = new System.Drawing.Point(18, 588);
            this.buttonL1NumerosImpares.Name = "buttonL1NumerosImpares";
            this.buttonL1NumerosImpares.Size = new System.Drawing.Size(168, 66);
            this.buttonL1NumerosImpares.TabIndex = 9;
            this.buttonL1NumerosImpares.Text = "Lista 10 Numero Impares";
            this.buttonL1NumerosImpares.UseVisualStyleBackColor = true;
            // 
            // buttonL2UnicoDigito
            // 
            this.buttonL2UnicoDigito.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonL2UnicoDigito.Location = new System.Drawing.Point(192, 12);
            this.buttonL2UnicoDigito.Name = "buttonL2UnicoDigito";
            this.buttonL2UnicoDigito.Size = new System.Drawing.Size(168, 66);
            this.buttonL2UnicoDigito.TabIndex = 10;
            this.buttonL2UnicoDigito.Text = "Lista 15 con Un Dígito";
            this.buttonL2UnicoDigito.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(192, 85);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 65);
            this.button1.TabIndex = 11;
            this.button1.Text = "Lista 15 con Dos Dígitos";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(192, 156);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(168, 65);
            this.button2.TabIndex = 12;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(192, 229);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(168, 65);
            this.button3.TabIndex = 13;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1181, 680);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonL2UnicoDigito);
            this.Controls.Add(this.buttonL1NumerosImpares);
            this.Controls.Add(this.buttonL1NumerosPares);
            this.Controls.Add(this.buttonL1InversoMetodoSacar);
            this.Controls.Add(this.buttonL1MetodoSacar);
            this.Controls.Add(this.buttonL2Inverso);
            this.Controls.Add(this.buttonL2Vacio);
            this.Controls.Add(this.buttonL1Vacio);
            this.Controls.Add(this.buttonL2);
            this.Controls.Add(this.buttonL1);
            this.Controls.Add(this.labelResultado);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.Name = "Form1";
            this.Text = "Ejercicio Clases";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonL1;
        private System.Windows.Forms.Button buttonL2;
        private System.Windows.Forms.Button buttonL1Vacio;
        private System.Windows.Forms.Button buttonL2Vacio;
        private System.Windows.Forms.Button buttonL2Inverso;
        private System.Windows.Forms.Button buttonL1MetodoSacar;
        private System.Windows.Forms.Button buttonL1InversoMetodoSacar;
        private System.Windows.Forms.Button buttonL1NumerosPares;
        private System.Windows.Forms.Button buttonL1NumerosImpares;
        private System.Windows.Forms.Button buttonL2UnicoDigito;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

