﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _06_WindowsFormsApplicationClasesEjer
{
    public partial class Form1 : Form
    {
        ListaNumeros lista1, lista2;
        public Form1()
        {
            InitializeComponent();
            lista1 = new ListaNumeros();
            lista2 = new ListaNumeros();
        }

        private void buttonL1_Click(object sender, EventArgs e)
        {
            Random num = new Random();
            labelResultado.Text = "El resultado es: \n\n";

            for (int i=0; i<10; i++)
            {
                int n1 = num.Next(0, 100);
                lista1.InsertarValor(n1);
                labelResultado.Text += n1 + "\n";
            }
        }

        private void buttonL2_Click(object sender, EventArgs e)
        {
            Random num = new Random();
            labelResultado.Text = "El resultado es: \n\n";

            for (int i = 0; i < 15; i++)
            {
                int n1 = num.Next(100);
                lista2.InsertarValor(n1);
                labelResultado.Text += n1 + "\n";
            }
        }

        private void buttonL1Vacio_Click(object sender, EventArgs e)
        {
            lista1.Vaciar();
            labelResultado.Text = "Se ha vaciado la lista de 10 números.";
        }

        private void buttonL2Vacio_Click(object sender, EventArgs e)
        {
            lista2.Vaciar();
            labelResultado.Text = "Se ha vaciado la lista de 15 números.";
        }

        private void buttonL2Inverso_Click(object sender, EventArgs e)
        {
            labelResultado.Text = "El resultado es: \n\n";

            int resultado;
            int contador = lista2.NumeroElementos();

            for (int i = contador; i > 0; i--)
            {
                resultado = lista2.Elemento(i);
                labelResultado.Text += resultado + "\n";
            }
        }
    }
}
