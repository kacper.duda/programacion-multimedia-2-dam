﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ConsoleApplicationTipos
{
    class Program
    {
        static void Main(string[] args)
        {
            Type tipo;
            double numero;
            char letra;

            tipo = typeof(string); // INDICA EL TIPO DE DATO A LA VARIABLE
            Console.WriteLine("El nombre corto es: " + tipo.Name);
            Console.WriteLine("El nombre largo es: " + tipo.FullName);

            if ((3.5 + 4) is Int32)
                Console.WriteLine("Es entero");
            else
                Console.WriteLine("No es entero");

            numero = 32.5;
            Console.WriteLine("El numero es " + numero);
            Console.WriteLine("El numero convertido es " + (int)numero);

            Console.ReadKey(); // SE QUEDA ESPERANDO A LA LECTURA DE UNA TECLA, SI NO ESTUVIESE ESTO, SE CERRARÍA EL PROGRAMA NADA MÁS EJECUTAR EL CÓDIGO ANTERIOR

            letra = 'e';
            switch(letra)
            {
                case 'a':
                    Console.WriteLine("Es vocal");
                    break;
                case 'e':
                    Console.WriteLine("Es vocal");
                    break;
                case 'b':
                    Console.WriteLine("No es vocal");
                    break;
                default:
                    Console.WriteLine("Es consonante");
                    break;
            }
        }
    }
}
