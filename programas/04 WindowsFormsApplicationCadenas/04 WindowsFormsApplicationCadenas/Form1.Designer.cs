﻿namespace _04_WindowsFormsApplicationCadenas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxCadena = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonInicializar = new System.Windows.Forms.Button();
            this.buttonEscribir = new System.Windows.Forms.Button();
            this.buttonInvertir = new System.Windows.Forms.Button();
            this.buttonMayusculas = new System.Windows.Forms.Button();
            this.buttonMinusculas = new System.Windows.Forms.Button();
            this.buttonDerecha = new System.Windows.Forms.Button();
            this.buttonI = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxCadena
            // 
            this.textBoxCadena.Location = new System.Drawing.Point(17, 16);
            this.textBoxCadena.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.textBoxCadena.Name = "textBoxCadena";
            this.textBoxCadena.Size = new System.Drawing.Size(260, 38);
            this.textBoxCadena.TabIndex = 0;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(318, 16);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(0, 31);
            this.labelResultado.TabIndex = 1;
            // 
            // buttonInicializar
            // 
            this.buttonInicializar.Location = new System.Drawing.Point(17, 88);
            this.buttonInicializar.Name = "buttonInicializar";
            this.buttonInicializar.Size = new System.Drawing.Size(195, 74);
            this.buttonInicializar.TabIndex = 2;
            this.buttonInicializar.Text = "Inicializar";
            this.buttonInicializar.UseVisualStyleBackColor = true;
            this.buttonInicializar.Click += new System.EventHandler(this.buttonInicializar_Click);
            // 
            // buttonEscribir
            // 
            this.buttonEscribir.Location = new System.Drawing.Point(218, 88);
            this.buttonEscribir.Name = "buttonEscribir";
            this.buttonEscribir.Size = new System.Drawing.Size(195, 74);
            this.buttonEscribir.TabIndex = 2;
            this.buttonEscribir.Text = "Escribir";
            this.buttonEscribir.UseVisualStyleBackColor = true;
            this.buttonEscribir.Click += new System.EventHandler(this.buttonEscribir_Click);
            // 
            // buttonInvertir
            // 
            this.buttonInvertir.Location = new System.Drawing.Point(419, 88);
            this.buttonInvertir.Name = "buttonInvertir";
            this.buttonInvertir.Size = new System.Drawing.Size(195, 74);
            this.buttonInvertir.TabIndex = 2;
            this.buttonInvertir.Text = "Invertir";
            this.buttonInvertir.UseVisualStyleBackColor = true;
            this.buttonInvertir.Click += new System.EventHandler(this.buttonInvertir_Click);
            // 
            // buttonMayusculas
            // 
            this.buttonMayusculas.Location = new System.Drawing.Point(620, 88);
            this.buttonMayusculas.Name = "buttonMayusculas";
            this.buttonMayusculas.Size = new System.Drawing.Size(195, 74);
            this.buttonMayusculas.TabIndex = 2;
            this.buttonMayusculas.Text = "Mayusculas";
            this.buttonMayusculas.UseVisualStyleBackColor = true;
            this.buttonMayusculas.Click += new System.EventHandler(this.buttonMayusculas_Click);
            // 
            // buttonMinusculas
            // 
            this.buttonMinusculas.Location = new System.Drawing.Point(821, 88);
            this.buttonMinusculas.Name = "buttonMinusculas";
            this.buttonMinusculas.Size = new System.Drawing.Size(195, 74);
            this.buttonMinusculas.TabIndex = 2;
            this.buttonMinusculas.Text = "Minusculas";
            this.buttonMinusculas.UseVisualStyleBackColor = true;
            this.buttonMinusculas.Click += new System.EventHandler(this.buttonMinusculas_Click);
            // 
            // buttonDerecha
            // 
            this.buttonDerecha.Location = new System.Drawing.Point(17, 168);
            this.buttonDerecha.Name = "buttonDerecha";
            this.buttonDerecha.Size = new System.Drawing.Size(195, 74);
            this.buttonDerecha.TabIndex = 2;
            this.buttonDerecha.Text = "Derecha";
            this.buttonDerecha.UseVisualStyleBackColor = true;
            this.buttonDerecha.Click += new System.EventHandler(this.buttonDerecha_Click);
            // 
            // buttonI
            // 
            this.buttonI.Location = new System.Drawing.Point(218, 168);
            this.buttonI.Name = "buttonI";
            this.buttonI.Size = new System.Drawing.Size(195, 74);
            this.buttonI.TabIndex = 2;
            this.buttonI.Text = "Izquierda";
            this.buttonI.UseVisualStyleBackColor = true;
            this.buttonI.Click += new System.EventHandler(this.buttonI_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 638);
            this.Controls.Add(this.buttonI);
            this.Controls.Add(this.buttonDerecha);
            this.Controls.Add(this.buttonMinusculas);
            this.Controls.Add(this.buttonMayusculas);
            this.Controls.Add(this.buttonInvertir);
            this.Controls.Add(this.buttonEscribir);
            this.Controls.Add(this.buttonInicializar);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxCadena);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxCadena;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonInicializar;
        private System.Windows.Forms.Button buttonEscribir;
        private System.Windows.Forms.Button buttonInvertir;
        private System.Windows.Forms.Button buttonMayusculas;
        private System.Windows.Forms.Button buttonMinusculas;
        private System.Windows.Forms.Button buttonDerecha;
        private System.Windows.Forms.Button buttonI;
    }
}

